package net.springboot.employee.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document (collection = "BranchDB")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Branch {
	
	@Id
	private long id;
	
	@NotBlank
    @Size(max=100)
	@Indexed(unique=true)
	private String name;
}
