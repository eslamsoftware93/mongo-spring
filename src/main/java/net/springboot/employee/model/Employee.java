package net.springboot.employee.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document (collection = "EmployeeDB")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
	
	@Id
	private long id;
	
	@NotBlank
    @Size(max=100)
	private String name;
	@NotBlank
	@Indexed(unique=true)
	private String nationalId;
	@NotBlank
	private int age;
}
