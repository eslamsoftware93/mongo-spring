package net.springboot.employee.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import net.springboot.employee.model.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, Long>{

}
