package net.springboot.employee.service;

import java.util.List;

import net.springboot.employee.model.Employee;

public interface EmployeeService {
	Employee createEmployee(Employee Employee);

	Employee updateEmployee(Employee Employee);

	List<Employee> getAllEmployee();

	Employee getEmployeeById(long EmployeeId);

	void deleteEmployee(long id);
}
