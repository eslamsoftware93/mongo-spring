package net.springboot.employee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.springboot.employee.exception.ResourceNotFoundException;
import net.springboot.employee.model.Employee;
import net.springboot.employee.repository.EmployeeRepository;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{

	
	@Autowired
	private EmployeeRepository EmployeeRepository;
	
	
	@Override
	public Employee createEmployee(Employee Employee) {
		return EmployeeRepository.save(Employee);
	}

	@Override
	public Employee updateEmployee(Employee Employee) {
		Optional<Employee> EmployeeDb = this.EmployeeRepository.findById(Employee.getId());
		
		if(EmployeeDb.isPresent()) {
			Employee EmployeeUpdate = EmployeeDb.get();
			EmployeeUpdate.setId(Employee.getId());
			EmployeeUpdate.setName(Employee.getName());
			EmployeeRepository.save(EmployeeUpdate);
			return EmployeeUpdate;
		}else {
			throw new ResourceNotFoundException("Record not found with id : " + Employee.getId());
		}		
	}

	@Override
	public List<Employee> getAllEmployee() {
		return this.EmployeeRepository.findAll();
	}

	@Override
	public Employee getEmployeeById(long EmployeeId) {
		
		Optional<Employee> EmployeeDb = this.EmployeeRepository.findById(EmployeeId);
		
		if(EmployeeDb.isPresent()) {
			return EmployeeDb.get();
		}else {
			throw new ResourceNotFoundException("Record not found with id : " + EmployeeId);
		}
	}

	@Override
	public void deleteEmployee(long EmployeeId) {
		Optional<Employee> EmployeeDb = this.EmployeeRepository.findById(EmployeeId);
		
		if(EmployeeDb.isPresent()) {
			this.EmployeeRepository.delete(EmployeeDb.get());
		}else {
			throw new ResourceNotFoundException("Record not found with id : " + EmployeeId);
		}
		
	}

}
