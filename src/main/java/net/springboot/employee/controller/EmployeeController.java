package net.springboot.employee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import net.springboot.employee.model.Employee;
import net.springboot.employee.service.EmployeeService;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	@Autowired
	private EmployeeService EmployeeService;
	
	@GetMapping("/employees")
	public ResponseEntity<List<Employee>> getAllEmployee(){
		return ResponseEntity.ok().body(EmployeeService.getAllEmployee());
	}
	
	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable long id){
		return ResponseEntity.ok().body(EmployeeService.getEmployeeById(id));
	}
	
	@PostMapping("/employees")
	public ResponseEntity<Employee> createEmployee(@RequestBody Employee Employee){
		return ResponseEntity.ok().body(this.EmployeeService.createEmployee(Employee));
	}
	
	@PutMapping("/employees/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable long id, @RequestBody Employee Employee){
		Employee.setId(id);
		return ResponseEntity.ok().body(this.EmployeeService.updateEmployee(Employee));
	}

	@DeleteMapping("/employees/{id}")
	public HttpStatus deleteEmployee(@PathVariable long id){
		this.EmployeeService.deleteEmployee(id);
		return HttpStatus.OK;
	}
}
