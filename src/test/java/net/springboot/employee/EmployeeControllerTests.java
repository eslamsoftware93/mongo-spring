package net.springboot.employee;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.springboot.employee.controller.EmployeeController;
import net.springboot.employee.model.Employee;
import net.springboot.employee.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTests {

    @MockBean
    private EmployeeRepository employeeRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldCreateEmployee() throws Exception {
        Employee employee = new Employee(1, "eslam", "789654321", 30);

        mockMvc.perform(post("/api/employees").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(employee)))
                .andExpect(status().isCreated())
                .andDo(print());
    }

    @Test
    void shouldReturnEmployee() throws Exception {
        long id = 1L;
        Employee employee = new Employee(id, "ali", "789654321", 13);

        when(employeeRepository.findById(id)).thenReturn(Optional.of(employee));
        mockMvc.perform(get("/api/employees/{id}", id)).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andDo(print());
    }

    @Test
    void shouldReturnNotFoundEmployee() throws Exception {
        long id = 1L;

        when(employeeRepository.findById(id)).thenReturn(Optional.empty());
        mockMvc.perform(get("/api/employees/{id}", id))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    void shouldReturnListOfEmployees() throws Exception {
        List<Employee> employees = new ArrayList<>(
                Arrays.asList(new Employee(1, "test1", "789654321", 13),
                        new Employee(2, "test2", "789654322", 45),
                        new Employee(3, "test3", "789654323", 22)));

        when(employeeRepository.findAll()).thenReturn(employees);
        mockMvc.perform(get("/api/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(employees.size()))
                .andDo(print());
    }


    @Test
    void shouldUpdateEmployee() throws Exception {
        long id = 1L;

        Employee employee = new Employee(id, "test4", "789654321", 7);
        Employee updatedemployee = new Employee(id, "Updated", "Updated", 5);

        when(employeeRepository.findById(id)).thenReturn(Optional.of(employee));
        when(employeeRepository.save(any(Employee.class))).thenReturn(updatedemployee);

        mockMvc.perform(put("/api/employees/{id}", id).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedemployee)))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    void shouldReturnNotFoundUpdateEmployee() throws Exception {
        long id = 1L;

        Employee updatedemployee = new Employee(id, "Updated", "Updated", 22);

        when(employeeRepository.findById(id)).thenReturn(Optional.empty());
        when(employeeRepository.save(any(Employee.class))).thenReturn(updatedemployee);

        mockMvc.perform(put("/api/employees/{id}", id).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedemployee)))
                .andExpect(status().isNotFound())
                .andDo(print());
    }

    @Test
    void shouldDeleteEmployee() throws Exception {
        long id = 1L;

        doNothing().when(employeeRepository).deleteById(id);
        mockMvc.perform(delete("/api/employees/{id}", id))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

    @Test
    void shouldDeleteAllEmployees() throws Exception {
        doNothing().when(employeeRepository).deleteAll();
        mockMvc.perform(delete("/api/employees"))
                .andExpect(status().isNoContent())
                .andDo(print());
    }

}
